import {NgModule} from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import {MaterialModule} from './material.module';
import {Animations} from '../front/animations/animations';
import {
  CalendarDateFormatter, CalendarModule, CalendarNativeDateFormatter,
  DateFormatterParams
} from 'angular-calendar';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CustomFormsModule} from 'ng2-validation';
import {HttpModule} from '@angular/http';
import {InlineSVGModule} from 'ng-inline-svg';
import {ToastyModule} from 'ng2-toasty';
import {NgxGalleryModule} from 'ngx-gallery';


export class CustomDateFormatter extends CalendarNativeDateFormatter {
  public dayViewHour({date, locale}: DateFormatterParams): string {
    return new Intl.DateTimeFormat('ca', {
      hour: 'numeric',
      minute: 'numeric'
    }).format(date);
  }
}


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    CustomFormsModule,
    HttpModule,
    MaterialModule,
    InlineSVGModule,
    NgxGalleryModule,
    CalendarModule.forRoot({
      dateFormatter: {
        provide: CalendarDateFormatter,
        useClass: CustomDateFormatter
      }
    }),
    ToastyModule.forRoot()
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    CustomFormsModule,
    HttpModule,
    MaterialModule,
    InlineSVGModule,
    NgxGalleryModule,
    CalendarModule,
    ToastyModule
  ],
  providers: [
    Animations,
    DatePipe
  ]
})
export class SharedModule {
}
