import {Component, HostBinding, OnInit} from '@angular/core';
import {Animations} from '../../animations/animations';

@Component({
  selector: 'app-services-article',
  templateUrl: './services-article.component.html',
  styleUrls: ['./services-article.component.scss'],
  animations: Animations.page
})
export class ServicesArticleComponent implements OnInit {
  @HostBinding('@routeAnimation') routeAnimation = true;

  constructor() { }

  ngOnInit() {
  }

}
