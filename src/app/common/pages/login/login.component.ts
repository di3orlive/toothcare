import {Component, HostBinding, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {CustomValidators} from 'ng2-validation';
import {Router} from '@angular/router';
import {Animations} from '../../../front/animations/animations';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  styles: [':host { display: block; }'],
  animations: Animations.page
})
export class LoginComponent implements OnInit {
  @HostBinding('@routeAnimation') routeAnimation = true;
  type = 'login';
  loginForm: FormGroup;
  registerForm: FormGroup;
  forgotForm: FormGroup;

  constructor(
    private router: Router,
    private fb: FormBuilder
  ) {
    const email = new FormControl('', [Validators.required, Validators.email]);
    const password = new FormControl('', Validators.required);
    const password2 = new FormControl('', [Validators.required, CustomValidators.equalTo(password)]);


    this.loginForm = this.fb.group({
      email: email,
      password: ['', Validators.required]
    });

    this.registerForm = this.fb.group({
      email: email,
      password: password,
      password2: password2
    });

    this.forgotForm = this.fb.group({
      email: email
    });
  }

  ngOnInit() {
  }


  login() {
    this.router.navigate(['dashboard/home']);
  }


  register() {

  }
}
