import {Component, HostBinding, OnInit} from '@angular/core';
import {Animations} from '../../animations/animations';
import {AppointmentComponent} from '../../pops/appointment/appointment.component';
import {MatDialog} from '@angular/material';


@Component({
  selector: 'app-diagnosis',
  templateUrl: './diagnosis.component.html',
  styleUrls: ['./diagnosis.component.scss'],
  animations: Animations.page
})
export class DiagnosisComponent implements OnInit {
  @HostBinding('@routeAnimation') routeAnimation = true;
  symptoms  = [
    [
      {
        symptom: 'Toothache',
        symptoms: [
          {
            symptom: 'Toothache after treatment',
            symptoms: [
              {symptom: 'Sealed up channels'},
              {symptom: 'Put seals'},
            ]
          },
          {symptom: 'pain occurs when brushing teeth'},
          {symptom: 'pain appeared after a blow to the face'},
          {symptom: 'The pain is worse when touched tooth'},
          {symptom: 'gnawing pain'},
          {symptom: 'responds to changes in temperature'}
        ]
      },
      {
        symptom: 'Sore and bleeding gum',
        symptoms: [
          {
            symptom: 'Toothache after treatment',
            symptoms: [
              {symptom: 'Sealed up channels'},
              {symptom: 'Put seals'},
            ]
          },
          {symptom: 'pain occurs when brushing teeth'},
          {symptom: 'pain appeared after a blow to the face'},
          {symptom: 'The pain is worse when touched tooth'},
          {symptom: 'gnawing pain'},
          {symptom: 'responds to changes in temperature'}
        ]
      },
      {
        symptom: 'Sore tooth extraction hole',
        symptoms: [
          {
            symptom: 'Toothache after treatment',
            symptoms: [
              {symptom: 'Sealed up channels'},
              {symptom: 'Put seals'},
            ]
          },
          {symptom: 'pain occurs when brushing teeth'},
          {symptom: 'pain appeared after a blow to the face'},
          {symptom: 'The pain is worse when touched tooth'},
          {symptom: 'gnawing pain'},
          {symptom: 'responds to changes in temperature'}
        ]
      },
      {
        symptom: 'Precipitated seal',
        symptoms: [
          {
            symptom: 'Toothache after treatment',
            symptoms: [
              {symptom: 'Sealed up channels'},
              {symptom: 'Put seals'},
            ]
          },
          {symptom: 'pain occurs when brushing teeth'},
          {symptom: 'pain appeared after a blow to the face'},
          {symptom: 'The pain is worse when touched tooth'},
          {symptom: 'gnawing pain'},
          {symptom: 'responds to changes in temperature'}
        ]
      },
      {
        symptom: 'Teeth curves, incorrectly positioned',
        symptoms: [
          {
            symptom: 'Toothache after treatment',
            symptoms: [
              {symptom: 'Sealed up channels'},
              {symptom: 'Put seals'},
            ]
          },
          {symptom: 'pain occurs when brushing teeth'},
          {symptom: 'pain appeared after a blow to the face'},
          {symptom: 'The pain is worse when touched tooth'},
          {symptom: 'gnawing pain'},
          {symptom: 'responds to changes in temperature'}
        ]
      },
      {
        symptom: 'Not like the shape of teeth',
        symptoms: [
          {
            symptom: 'Toothache after treatment',
            symptoms: [
              {symptom: 'Sealed up channels'},
              {symptom: 'Put seals'},
            ]
          },
          {symptom: 'pain occurs when brushing teeth'},
          {symptom: 'pain appeared after a blow to the face'},
          {symptom: 'The pain is worse when touched tooth'},
          {symptom: 'gnawing pain'},
          {symptom: 'responds to changes in temperature'}
        ]
      },
      {
        symptom: 'Not like the color of teeth',
        symptoms: [
          {
            symptom: 'Toothache after treatment',
            symptoms: [
              {symptom: 'Sealed up channels'},
              {symptom: 'Put seals'},
            ]
          },
          {symptom: 'pain occurs when brushing teeth'},
          {symptom: 'pain appeared after a blow to the face'},
          {symptom: 'The pain is worse when touched tooth'},
          {symptom: 'gnawing pain'},
          {symptom: 'responds to changes in temperature'}
        ]
      },
      {
        symptom: 'Bad breath',
        symptoms: [
          {
            symptom: 'Toothache after treatment',
            symptoms: [
              {symptom: 'Sealed up channels'},
              {symptom: 'Put seals'},
            ]
          },
          {symptom: 'pain occurs when brushing teeth'},
          {symptom: 'pain appeared after a blow to the face'},
          {symptom: 'The pain is worse when touched tooth'},
          {symptom: 'gnawing pain'},
          {symptom: 'responds to changes in temperature'}
        ]
      },
      {
        symptom: 'Baring the roots of teeth',
        symptoms: [
          {
            symptom: 'Toothache after treatment',
            symptoms: [
              {symptom: 'Sealed up channels'},
              {symptom: 'Put seals'},
            ]
          },
          {symptom: 'pain occurs when brushing teeth'},
          {symptom: 'pain appeared after a blow to the face'},
          {symptom: 'The pain is worse when touched tooth'},
          {symptom: 'gnawing pain'},
          {symptom: 'responds to changes in temperature'}
        ]
      },
      {
        symptom: 'Broke away portion of the tooth',
        symptoms: [
          {
            symptom: 'Toothache after treatment',
            symptoms: [
              {symptom: 'Sealed up channels'},
              {symptom: 'Put seals'},
            ]
          },
          {symptom: 'pain occurs when brushing teeth'},
          {symptom: 'pain appeared after a blow to the face'},
          {symptom: 'The pain is worse when touched tooth'},
          {symptom: 'gnawing pain'},
          {symptom: 'responds to changes in temperature'}
        ]
      },
      {
        symptom: 'Missing tooth (teeth)',
        symptoms: [
          {
            symptom: 'Toothache after treatment',
            symptoms: [
              {symptom: 'Sealed up channels'},
              {symptom: 'Put seals'},
            ]
          },
          {symptom: 'pain occurs when brushing teeth'},
          {symptom: 'pain appeared after a blow to the face'},
          {symptom: 'The pain is worse when touched tooth'},
          {symptom: 'gnawing pain'},
          {symptom: 'responds to changes in temperature'}
        ]
      },
      {
        symptom: 'Appeared tooth mobility',
        symptoms: [
          {
            symptom: 'Toothache after treatment',
            symptoms: [
              {symptom: 'Sealed up channels'},
              {symptom: 'Put seals'},
            ]
          },
          {symptom: 'pain occurs when brushing teeth'},
          {symptom: 'pain appeared after a blow to the face'},
          {symptom: 'The pain is worse when touched tooth'},
          {symptom: 'gnawing pain'},
          {symptom: 'responds to changes in temperature'}
        ]
      },
      {
        symptom: 'Appeared cheeks edema (flux)',
        symptoms: [
          {
            symptom: 'Toothache after treatment',
            symptoms: [
              {symptom: 'Sealed up channels'},
              {symptom: 'Put seals'},
            ]
          },
          {symptom: 'pain occurs when brushing teeth'},
          {symptom: 'pain appeared after a blow to the face'},
          {symptom: 'The pain is worse when touched tooth'},
          {symptom: 'gnawing pain'},
          {symptom: 'responds to changes in temperature'}
        ]
      }
    ]
  ];



  constructor(
    public dialog: MatDialog
  ) {}



  ngOnInit() {
  }

  setSymptom(val) {
    this.symptoms.push(val);
  }

  backSymptom() {
    this.symptoms.pop();
  }

  resetSymptoms() {
    this.symptoms.splice(1);
  }

  appointmentPop() {
    this.dialog.open(AppointmentComponent);
  }
}
