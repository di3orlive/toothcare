import {Component, OnInit, ViewChild} from '@angular/core';
import {MatSidenav} from '@angular/material';
import {SideNavService} from './services/side-nav/side-nav.service';

@Component({
  selector: 'app-front',
  templateUrl: './front.component.html'
})
export class FrontMainComponent implements OnInit {
  @ViewChild('sidenav') sidenav: MatSidenav;

  constructor(
    private sideNavService: SideNavService
  ) { }

  ngOnInit() {
    this.sideNavService.setSideNav(this.sidenav);
  }


  onDeactivate() {
    document.body.scrollTop = 0;
  }
}
