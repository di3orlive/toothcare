import {Component, OnInit} from '@angular/core';
import {AppointmentComponent} from '../../pops/appointment/appointment.component';
import {MatDialog} from '@angular/material';
import {CallMeComponent} from '../../pops/call-me/call-me.component';
import {SideNavService} from '../../services/side-nav/side-nav.service';
import {SafeSubscribe} from '../../helpers/safe-subscripe/safe-subscripe';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent extends SafeSubscribe implements OnInit {

  constructor(
    public dialog: MatDialog,
    private sideNavService: SideNavService
  ) {
    super();
  }

  ngOnInit() {
  }

  appointmentPop() {
    this.dialog.open(AppointmentComponent);
  }

  callMePop() {
    this.dialog.open(CallMeComponent);
  }

  toggleSideNav() {
    this.sideNavService.toggle().then(() => { });
  }
}
