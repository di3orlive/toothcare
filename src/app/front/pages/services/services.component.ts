import {Component, HostBinding, OnInit} from '@angular/core';
import {Animations} from '../../animations/animations';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styles: [':host { display: block; }'],
  animations: Animations.page
})
export class ServicesComponent implements OnInit {
  @HostBinding('@routeAnimation') routeAnimation = true;

  constructor() { }

  ngOnInit() {
  }

}
