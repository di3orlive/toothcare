import {Component, HostBinding, OnInit} from '@angular/core';
import {Animations} from '../../animations/animations';

@Component({
  selector: 'app-educational',
  templateUrl: './educational.component.html',
  styleUrls: ['./educational.component.scss'],
  animations: Animations.page
})
export class EducationalComponent implements OnInit {
  @HostBinding('@routeAnimation') routeAnimation = true;

  constructor() { }

  ngOnInit() {
  }

}
