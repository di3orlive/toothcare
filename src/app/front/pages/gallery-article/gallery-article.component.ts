import {Component, OnInit, OnDestroy, HostBinding} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Animations} from '../../animations/animations';

@Component({
  selector: 'app-gallery-article',
  templateUrl: './gallery-article.component.html',
  styleUrls: ['./gallery-article.component.scss'],
  animations: Animations.page
})
export class GalleryArticleComponent implements OnInit, OnDestroy {
  @HostBinding('@routeAnimation') routeAnimation = true;
  name: any;
  private subOnParams: any;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.subOnParams = this.route.params.subscribe(params => {
      this.name = params['name'];
    });
  }

  ngOnDestroy() {
    this.subOnParams.unsubscribe();
  }
}
