import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BackSettingsComponent } from './settings.component';

describe('BackSettingsComponent', () => {
  let component: BackSettingsComponent;
  let fixture: ComponentFixture<BackSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BackSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BackSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
