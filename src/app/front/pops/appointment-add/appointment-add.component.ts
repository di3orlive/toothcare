import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-appointment-add',
  templateUrl: './appointment-add.component.html',
  styleUrls: ['./appointment-add.component.scss']
})
export class AppointmentAddComponent implements OnInit {
  add = {
    name: '',
    phone: '',
    email: '',
    additional: ''
  };
  dateStart: Date;
  dateEnd: Date;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<AppointmentAddComponent>
  ) {
    this.dateStart = this.data;
  }

  ngOnInit() {
  }


  save(f) {
    if (f.invalid) {
      return;
    }



    // colorId = https://eduardopereira.pt/wp-content/uploads/2012/06/google_calendar_api_event_color_chart.png
    const body = {
      location: null,
      status: 'confirmed',
      colorId: 10,
      summary: 'reserved', // this.selectedService.summary
      description: `name: ${this.add.name}
${this.add.phone ? 'phone: ' + this.add.phone : ''}
${this.add.email ? 'email: ' + this.add.email : ''}
${this.add.additional ? 'additional: ' + this.add.additional : ''}`,
      start: {dateTime: new Date(this.dateStart).toISOString()},
      end: {dateTime: new Date(new Date(this.data.setHours(this.data.getHours() + 1))).toISOString()} // this.dateEnd
    };


    // this.contacts.forEach((item) => {
    //   if (item.id === this.masseur) {
    //     body.location = item.address;
    //   }
    // });

    console.log(body);

    this.dialogRef.close(body);
  }

  close() {
    this.dialogRef.close();
  }
}
