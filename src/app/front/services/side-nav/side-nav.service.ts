import { Injectable } from '@angular/core';
import {MatSidenav} from '@angular/material';

@Injectable()
export class SideNavService {
  private sideNav: MatSidenav;

  public setSideNav(sideNav: MatSidenav) {
    this.sideNav = sideNav;
  }

  public open(): Promise<any> {
    return this.sideNav.open();
  }

  public close(): Promise<any> {
    return this.sideNav.close();
  }

  public toggle(isOpen?: boolean): Promise<any> {
    return this.sideNav.toggle(isOpen);
  }
}
