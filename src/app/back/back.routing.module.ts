import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {BackMainComponent} from './back.component';
import {BackHomeComponent} from './pages/home/home.component';
import {BackHistoryComponent} from './pages/history/history.component';
import {BackSettingsComponent} from './pages/settings/settings.component';

const routes: Routes = [
  {
    path: '',
    component: BackMainComponent,
    children: [
      {path: 'home', component: BackHomeComponent},
      {path: 'history', component: BackHistoryComponent},
      {path: 'settings', component: BackSettingsComponent},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BackRoutingModule {
}



