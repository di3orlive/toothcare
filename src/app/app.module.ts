import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {AppComponent} from './app.component';
import {RoutingModule} from './app.routing.module';
import {LoginComponent} from './common/pages/login/login.component';
import {PageNotFoundComponent} from './common/pages/page-not-found/page-not-found.component';
import {SharedModule} from './shared/shared.module';




@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    RoutingModule,
    BrowserAnimationsModule,
    SharedModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}

