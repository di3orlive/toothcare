import {Component, HostBinding, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material';
import {Animations} from '../../animations/animations';
import {AppointmentComponent} from '../../pops/appointment/appointment.component';

declare let google: any;

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styles: [':host { display: block; }'],
  animations: Animations.page
})
export class ContactsComponent implements OnInit {
  @HostBinding('@routeAnimation') routeAnimation = true;
  map: any;

  constructor(
    public dialog: MatDialog
  ) {}

  ngOnInit() {
  }


  appointmentPop() {
    this.dialog.open(AppointmentComponent);
  }


  initMap() {
    this.map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: 25.2516302, lng: 55.3014181},
      zoom: 14,
      scrollwheel: false
    });

    const marker = new google.maps.Marker({
      position: {lat: 25.2516302, lng: 55.3014181},
      map: this.map,
      title: 'Dubai Sky Clinic'
    });

    return this.map;
  }

}
