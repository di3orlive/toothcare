import {NgModule} from '@angular/core';
import {SharedModule} from '../shared/shared.module';
import {BackRoutingModule} from './back.routing.module';
import {BackHomeComponent} from './pages/home/home.component';
import {BackMainComponent} from './back.component';
import {BackHistoryComponent} from './pages/history/history.component';
import {BackSettingsComponent} from './pages/settings/settings.component';
import {TeethComponent} from './components/teeth/teeth.component';

@NgModule({
  imports: [
    SharedModule,
    BackRoutingModule
  ],
  declarations: [
    BackHomeComponent,
    BackMainComponent,
    BackHistoryComponent,
    BackSettingsComponent,
    TeethComponent
  ]
})
export class BackModule {
}
