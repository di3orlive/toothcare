import {Component} from '@angular/core';
import {MatDialogRef} from '@angular/material';
import {FormBuilder, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-call-me',
  templateUrl: './call-me.component.html',
  styleUrls: ['./call-me.component.scss']
})
export class CallMeComponent {
  form: FormGroup;

  constructor(
    private dialogRef: MatDialogRef<CallMeComponent>,
    private fb: FormBuilder
  ) {
    this.createForm();
  }

  createForm() {
    this.form = this.fb.group({
      phone: '',
      name: '',
    });
  }

  close() {
    this.dialogRef.close();
  }
}
