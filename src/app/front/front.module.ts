import {NgModule} from '@angular/core';
import {SharedModule} from '../shared/shared.module';
import {FrontRoutingModule} from './front.routing.module';
import {HomeComponent} from './pages/home/home.component';
import {ServicesComponent} from './pages/services/services.component';
import {TeamComponent} from './pages/team/team.component';
import {DiagnosisComponent} from './pages/diagnosis/diagnosis.component';
import {GalleryComponent} from './pages/gallery/gallery.component';
import {EducationalComponent} from './pages/educational/educational.component';
import {How2careComponent} from './pages/how2care/how2care.component';
import {ContactsComponent} from './pages/contacts/contacts.component';
import {HeaderComponent} from './components/header/header.component';
import {FooterComponent} from './components/footer/footer.component';
import {MapComponent} from './components/map/map.component';
import {InViewportDirective} from './directives/in-viewport/in-viewport.directive';
import {GalleryArticleComponent} from './pages/gallery-article/gallery-article.component';
import {TeamArticleComponent} from './pages/team-article/team-article.component';
import {ServicesArticleComponent} from './pages/services-article/services-article.component';
import {AppointmentComponent} from './pops/appointment/appointment.component';
import {AppointmentAddComponent} from './pops/appointment-add/appointment-add.component';
import {CallMeComponent} from './pops/call-me/call-me.component';
import {FrontMainComponent} from './front.component';
import {CalendarService} from './services/calendar/calendar.service';
import {MenuComponent} from './components/menu/menu.component';
import {SideNavService} from './services/side-nav/side-nav.service';

@NgModule({
  imports: [
    SharedModule,
    FrontRoutingModule
  ],
  declarations: [
    HomeComponent,
    ServicesComponent,
    TeamComponent,
    DiagnosisComponent,
    GalleryComponent,
    EducationalComponent,
    How2careComponent,
    ContactsComponent,
    HeaderComponent,
    FooterComponent,
    MapComponent,
    InViewportDirective,
    GalleryArticleComponent,
    TeamArticleComponent,
    ServicesArticleComponent,
    AppointmentComponent,
    AppointmentAddComponent,
    CallMeComponent,
    FrontMainComponent,
    MenuComponent
  ],
  entryComponents: [
    AppointmentComponent,
    AppointmentAddComponent,
    CallMeComponent
  ],
  providers: [
    CalendarService,
    SideNavService
  ]
})
export class FrontModule {
}
