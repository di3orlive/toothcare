import {Component, HostBinding, OnInit} from '@angular/core';
import {Animations} from '../../animations/animations';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styles: [':host { display: block; }'],
  animations: Animations.page
})
export class TeamComponent implements OnInit {
  @HostBinding('@routeAnimation') routeAnimation = true;
  team = [
    {ava: 'assets/i/doctor/1.png', name: 'Andrew	Lindsey'},
    {ava: 'assets/i/doctor/2.png', name: 'Cathy	Padilla'},
    {ava: 'assets/i/doctor/3.png', name: 'Judy	Jefferson'},
    {ava: 'assets/i/doctor/4.png', name: 'Donnie	Sims'},
    {ava: 'assets/i/doctor/5.png', name: 'Daniel	Francis'},
    {ava: 'assets/i/doctor/6.png', name: 'Lynda	Wilson'},
    {ava: 'assets/i/doctor/7.png', name: 'Marcos	Moody'}
  ];

  constructor() { }

  ngOnInit() {
  }
}
