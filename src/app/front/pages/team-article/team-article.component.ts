import {Component, HostBinding, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {NgxGalleryAnimation, NgxGalleryOptions, NgxGalleryImage} from 'ngx-gallery';
import {Animations} from '../../animations/animations';

@Component({
  selector: 'app-team-article',
  templateUrl: './team-article.component.html',
  styleUrls: ['./team-article.component.scss'],
  animations: Animations.page
})
export class TeamArticleComponent implements OnInit, OnDestroy {
  @HostBinding('@routeAnimation') routeAnimation = true;
  certificatesGallery: NgxGalleryImage[];
  galleryOptions: NgxGalleryOptions[];
  doctorGallery: NgxGalleryImage[];
  name: number;
  private subOnParams: any;

  constructor(private route: ActivatedRoute) {

    this.galleryOptions = [{
        width: '550px',
        height: '400px',
        thumbnailsColumns: 4,
        imageAnimation: NgxGalleryAnimation.Slide,
        preview: false
      },
      {
        breakpoint: 800,
        width: '100%',
        height: '600px',
        imagePercent: 80,
        thumbnailsPercent: 20,
        thumbnailsMargin: 20,
        thumbnailMargin: 20
      },
      {
        breakpoint: 400,
        preview: false
    }];

    this.doctorGallery = [
      {
        small: 'assets/i/doctor/gallery/1.jpg',
        medium: 'assets/i/doctor/gallery/1.jpg',
        big: 'assets/i/doctor/gallery/1.jpg'
      },
      {
        small: 'assets/i/doctor/gallery/2.jpg',
        medium: 'assets/i/doctor/gallery/2.jpg',
        big: 'assets/i/doctor/gallery/2.jpg'
      },
      {
        small: 'assets/i/doctor/gallery/3.jpg',
        medium: 'assets/i/doctor/gallery/3.jpg',
        big: 'assets/i/doctor/gallery/3.jpg'
      },
      {
        small: 'assets/i/doctor/gallery/4.jpg',
        medium: 'assets/i/doctor/gallery/4.jpg',
        big: 'assets/i/doctor/gallery/4.jpg'
      },
      {
        small: 'assets/i/doctor/gallery/5.jpg',
        medium: 'assets/i/doctor/gallery/5.jpg',
        big: 'assets/i/doctor/gallery/5.jpg'
      },
      {
        small: 'assets/i/doctor/gallery/2.jpg',
        medium: 'assets/i/doctor/gallery/2.jpg',
        big: 'assets/i/doctor/gallery/2.jpg'
      },
      {
        small: 'assets/i/doctor/gallery/3.jpg',
        medium: 'assets/i/doctor/gallery/3.jpg',
        big: 'assets/i/doctor/gallery/3.jpg'
      },
      {
        small: 'assets/i/doctor/gallery/4.jpg',
        medium: 'assets/i/doctor/gallery/4.jpg',
        big: 'assets/i/doctor/gallery/4.jpg'
      },
      {
        small: 'assets/i/doctor/gallery/5.jpg',
        medium: 'assets/i/doctor/gallery/5.jpg',
        big: 'assets/i/doctor/gallery/5.jpg'
      },
      {
        small: 'assets/i/doctor/gallery/2.jpg',
        medium: 'assets/i/doctor/gallery/2.jpg',
        big: 'assets/i/doctor/gallery/2.jpg'
      },
      {
        small: 'assets/i/doctor/gallery/3.jpg',
        medium: 'assets/i/doctor/gallery/3.jpg',
        big: 'assets/i/doctor/gallery/3.jpg'
      },
      {
        small: 'assets/i/doctor/gallery/4.jpg',
        medium: 'assets/i/doctor/gallery/4.jpg',
        big: 'assets/i/doctor/gallery/4.jpg'
      },
      {
        small: 'assets/i/doctor/gallery/5.jpg',
        medium: 'assets/i/doctor/gallery/5.jpg',
        big: 'assets/i/doctor/gallery/5.jpg'
      }
    ];

    this.certificatesGallery = [
      {
        small: 'assets/i/doctor/certificates/1.jpg',
        medium: 'assets/i/doctor/certificates/1.jpg',
        big: 'assets/i/doctor/certificates/1.jpg'
      },
      {
        small: 'assets/i/doctor/certificates/2.jpg',
        medium: 'assets/i/doctor/certificates/2.jpg',
        big: 'assets/i/doctor/certificates/2.jpg'
      },
      {
        small: 'assets/i/doctor/certificates/3.jpg',
        medium: 'assets/i/doctor/certificates/3.jpg',
        big: 'assets/i/doctor/certificates/3.jpg'
      },
      {
        small: 'assets/i/doctor/certificates/4.jpg',
        medium: 'assets/i/doctor/certificates/4.jpg',
        big: 'assets/i/doctor/certificates/4.jpg'
      },
      {
        small: 'assets/i/doctor/certificates/5.jpg',
        medium: 'assets/i/doctor/certificates/5.jpg',
        big: 'assets/i/doctor/certificates/5.jpg'
      },
      {
        small: 'assets/i/doctor/certificates/2.jpg',
        medium: 'assets/i/doctor/certificates/2.jpg',
        big: 'assets/i/doctor/certificates/2.jpg'
      },
      {
        small: 'assets/i/doctor/certificates/3.jpg',
        medium: 'assets/i/doctor/certificates/3.jpg',
        big: 'assets/i/doctor/certificates/3.jpg'
      },
      {
        small: 'assets/i/doctor/certificates/4.jpg',
        medium: 'assets/i/doctor/certificates/4.jpg',
        big: 'assets/i/doctor/certificates/4.jpg'
      },
      {
        small: 'assets/i/doctor/certificates/5.jpg',
        medium: 'assets/i/doctor/certificates/5.jpg',
        big: 'assets/i/doctor/certificates/5.jpg'
      }
    ];



  }

  ngOnInit() {
    this.subOnParams = this.route.params.subscribe(params => {
      this.name = params['name'];
    });
  }

  ngOnDestroy() {
    this.subOnParams.unsubscribe();
  }
}
