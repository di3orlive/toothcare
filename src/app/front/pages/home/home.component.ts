import {Component, HostBinding, OnInit} from '@angular/core';
import {Animations} from '../../animations/animations';
import {MatDialog} from '@angular/material';
import {AppointmentComponent} from '../../pops/appointment/appointment.component';

declare let google: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  animations: Animations.page
})
export class HomeComponent implements OnInit {
  @HostBinding('@routeAnimation') routeAnimation = true;
  num1 = 0;
  num2 = 0;
  num3 = 0;
  runOneTime = true;


  constructor(
    public dialog: MatDialog
  ) {}

  ngOnInit() {


  }


  initMap() {
    return new google.maps.StreetViewPanorama(
      document.getElementById('map'), {
        position: {lat: 25.2510787, lng: 55.3015099},
        pov: {heading: 100, pitch: 0},
        zoom: 0
      });
  }


  inViewport(inView) {
    if (inView) {
      if (this.runOneTime) {
        this.runOneTime = false;

        const num1 = setInterval(() => {
          this.num1 += 1;
          if (this.num1 >= 1000) {
            clearInterval(num1);
          }
        }, 10 / 2);


        const num2 = setInterval(() => {
          this.num2 += 1;
          if (this.num2 >= 1500) {
            clearInterval(num2);
          }
        }, 7 / 2);


        const num3 = setInterval(() => {
          this.num3 += 1;
          if (this.num3 >= 700) {
            clearInterval(num3);
          }
        }, 15 / 2);
      }
    }
  }


  appointmentPop() {
    this.dialog.open(AppointmentComponent);
  }
}
