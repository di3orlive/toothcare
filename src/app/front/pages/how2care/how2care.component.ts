import {Component, HostBinding, OnInit} from '@angular/core';
import {Animations} from '../../animations/animations';

@Component({
  selector: 'app-how2care',
  templateUrl: './how2care.component.html',
  styles: [':host { display: block; }'],
  animations: Animations.page
})
export class How2careComponent implements OnInit {
  @HostBinding('@routeAnimation') routeAnimation = true;

  constructor() { }

  ngOnInit() {
  }

}
